<?php
use Stone\App;

require_once dirname(__DIR__).'/vendor/autoload.php';

$app = App::create();
$app->run();
import {icon} from './icons.js';
document.querySelector('#copyHTML').addEventListener('click', function (e) {
	let iconContainer = e.currentTarget.querySelector('.icon');
	navigator.clipboard.writeText(document.querySelector('#content').innerHTML.trim()).then(() => {
		iconContainer.innerHTML = icon('clipboard-check');
		setTimeout(
			() => {
				iconContainer.innerHTML = icon('clipboard');
			},
			3000
		);
	}, function() {
  		iconContainer.innerHTML = icon('clipboard-x');
		setTimeout(
			() => {
				iconContainer.innerHTML = icon('clipboard');
			},
			3000
		);
	});
});

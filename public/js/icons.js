export let icon = (name, width = 18, height = 18, color = 'currentColor') => {
	return `
		<svg class="bi" width="${width}" height="${height}" fill="${color}">
	  		<use xlink:href="${iconUrl}/icons.svg#${name}"/>
		</svg>`;
}
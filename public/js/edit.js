var editor = CodeMirror.fromTextArea(document.getElementById('editor'), {
	lineNumbers: true,
	mode: 'markdown',
	lineWrapping: true
});

editor.setSize('100%', '80vh');

editor.on('change', (editor) => {
	editor.save();
});

markdownDB = (action, callback = () => {}) => {
	if (window.indexedDB) {
		let documentDB = window.indexedDB.open('documents');

		documentDB.onerror = (event) => {
			let db = event.target.result
			console.log("Error opening database: " + db);
		};

		documentDB.onsuccess = function (event) {
			let db = event.target.result;
			console.log('Database: ' + db + ' is ready!');
			let documentStore = db.transaction("document", "readwrite").objectStore("document");
			if (action == 'put') {
				let request = documentStore.put({
					id: 'markdown',
					content: callback()
				});

				request.onerror = () => {
					callback(false);
				}

				request.onsuccess = () => {
					callback(true);
				};
			} else if (action == 'get') {
				let request = documentStore.get('markdown');

				request.onerror = () => {
					callback(false);
				}

				request.onsuccess = () => {
					callback(request.result.content);
				};
			}

		};

		documentDB.onupgradeneeded = (event) => {
			let db = event.target.result;
			console.log('Creating: ' + db + ' DB structure!');
			if (!db.objectStoreNames.contains('document')) {
				db.createObjectStore('document', { keyPath: 'id' });
			}
		};
	} else {
		console.log("Your browser doesn't support a stable version of IndexedDB. So the content will be always saved every each 6s no matter if it did not change");
	}
}

/*const editor = new toastui.Editor({
	el: document.querySelector('#editor'),
	height: '500px',
	initialEditType: 'markdown',
	initialValue: document.querySelector('#content').value,
	previewStyle: 'tab',
	usageStatistics: false,
	events: {
		change: function() {
			document.querySelector('#content').value = editor.getMarkdown();
		}
	}
});*/

// POST method implementation:
async function postData(url = '', formData) {
	let headers = new Headers();
	// headers.append('Content-Type', 'application/x-www-form-urlencoded');
	headers.append('X-Requested-With', 'XMLHttpRequest');
	// Default options are marked with *
	const response = await fetch(url, {
		method: 'POST', // *GET, POST, PUT, DELETE, etc.
		mode: 'same-origin', // no-cors, *cors, same-origin
		cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
		credentials: 'same-origin', // include, *same-origin, omit
		headers: headers,
		redirect: 'follow', // manual, *follow, error
		referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
		body: formData // body data type must match "Content-Type" header
	});
	return response.json(); // parses JSON response into native JavaScript objects
};

form = document.querySelector('form');
url = form.getAttribute('action');

//document.querySelectorAll('input[data-role="data"]').forEach((e) => {e.value);})

markdownDB('put', () => {
	return document.getElementById('editor').value;
});

setInterval(
	function() {
		let title = document.getElementById('title').value;
		let currentContent = document.getElementById('editor').value;
		let save = (title != '') && (currentContent != '');
		if (save) {
			markdownDB('get', (savedContent) => {
				if (savedContent != currentContent) {
					console.log('Autosaving');
					postData(url, new FormData(form)).then(
						data => {
							if (data.result == 'OK') {
								document.getElementById('action').value = 'EDIT';
								document.getElementById('id').value = data._id;
								markdownDB('put', () => {
									return document.getElementById('editor').value;
								});
							}
						}
					);
				} else {
					console.log('Contents are identical no saving!!!');
				}
			});
		}
	},
	5000
);

<?php

namespace Stone\DB;

class TranslationsRepository extends Repository
{
    protected function __construct()
    {
        parent::__construct('translations');
    }

    public function add(array $translation): int
    {
        if (isset($translation['_id'])) {
            $oldTranslations = $this->findById($translation['_id']);
        } else {
            $language = key($translation);
            $slug = $translation[$language];
            $oldTranslations = $this->findOneBy([$language, '=', $slug]);
        }

        if (!empty($oldTranslations)) {
            // Store the timestamp in UTC
            $translation['updatedAt'] = time();
            $translations = array_merge($oldTranslations, $translation);
            if ($this->storage->update($translations)) {
                return $translations['_id'];
            }
            return 0;
        }
        // Clean any id attribute before inserting
        unset($translation['_id']);
        // Store the timestamp in UTC
        $translation['createdAt'] = time();
        $translation = $this->storage->insert($translation);
        return $translation['_id'];
    }

    public function getAll($limit = 10, array $except = [])
    {
        $postQueryBuilder = $this->storage->createQueryBuilder();

        $translations = $postQueryBuilder
            ->except($except)
            ->orderBy(['updatedAt' => 'desc'])
            ->limit($limit)
            ->getQuery()
            ->fetch();

        return $translations;
    }
}

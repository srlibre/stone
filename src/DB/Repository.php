<?php
namespace Stone\DB;

use Stone\Traits\SingletonTrait;
use SleekDB\Store;
use SleekDB\Query;

class Repository
{
    use SingletonTrait;

    /** @var Store */
    protected $storage;

    protected function __construct($storageName)
    {
        $this->storage = new Store($storageName, DB::getDBDir(), ['timeout' => false]);
    }

    public function findById($id)
    {
        return $this->storage->findById($id);
    }

    public function findOneBy(array $criteria, array $except = [])
    {
        if (!empty($except)) {
            $qb = $this->storage->createQueryBuilder();

            $qb->except($except);

            $qb->where($criteria);

            $result = $qb->getQuery()->first();

            return (!empty($result))? $result : null;
        } else {
            return $this->storage->findOneBy($criteria);
        }
    }

    public function findByCriteria(array $criteria)
    {
        return $this->storage->findOneBy($criteria);
    }

    public function deleteById(int $id): bool
    {
        return $this->storage->deleteById($id);
    }

    public function deleteByCriteria(array $criteria)
    {
        return $this->storage->deleteBy($criteria, Query::DELETE_RETURN_RESULTS);
    }
}

<?php

namespace Stone\DB;

class DB 
{
    protected static $dbDir;
    
    public static function setDBDir(string $dbDir) 
    {
        self::$dbDir = $dbDir;
    }
    
    public static function getDBDir() 
    {
        return self::$dbDir;
    }
    
    public static function getMetadataRepository(): MetadataRepository
    {
        return MetadataRepository::create();
    }
    
    public static function getTranslationsRepository(): TranslationsRepository
    {
        return TranslationsRepository::create();
    }
}
<?php

namespace Stone\DB;

class MetadataRepository extends Repository
{
    protected function __construct()
    {
        parent::__construct('metadata');
    }

    public function add(array $metadata) : int
    {
        if (!empty($metadata['_id'])) {
            $oldMetadata = $this->findById($metadata['_id']);
        } else {
            $oldMetadata = $this->findByCriteria(['slug', '=', $metadata['slug']]);
        }

        if (!empty($oldMetadata)) {
            // Store the timestamp in UTC
            $metadata['updatedAt'] = time();
            $metadata = array_merge($oldMetadata, $metadata);
            if ($this->storage->update($metadata)) {
                return $metadata['_id'];
            }
            return 0;
        }
        // Clean any id attribute before inserting
        unset($metadata['_id']);
        // Store the timestamp in UTC
        $metadata['createdAt'] = time();
        $metadata = $this->storage->insert($metadata);
        return $metadata['_id'];
    }

    public function getAll($limit = 10, array $conditions = [])
    {
        $metadataStore = $this->storage;
        $translationsStore = DB::getTranslationsRepository();

        $metadata = $metadataStore
            ->createQueryBuilder()
            ->where($conditions)
            ->limit($limit)
            ->join(
                function($metadata) use ($translationsStore) {
                    $translations = $translationsStore->findOneBy([$metadata['language'], "=", $metadata['slug']], ['_id', 'createdAt', 'updatedAt']);
                    return !empty($translations) && is_array($translations) ? $translations : [];  
                }, 
                'translations'
            )
            ->orderBy(['updatedAt' => 'desc'])
            ->getQuery()
            ->fetch();

        return $metadata;
    }

    public function deleteByCriteria(array $criteria)
    {
        if ($markdownMetadata = $this->findByCriteria($criteria)) {
            $translationsId = $markdownMetadata['translationsId'];
            if ($deleted = parent::deleteByCriteria(['translationsId', '=', $translationsId])) {
                DB::getTranslationsRepository()->deleteById($translationsId);
                return $deleted;
            }
        }
        return [];
    }
}

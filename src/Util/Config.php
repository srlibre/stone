<?php
namespace Stone\Util;

use Stone\Traits\SingletonTrait;
use Symfony\Component\Dotenv\Dotenv;
use Stone\Traits\RegistryTrait;

class Config
{
    use SingletonTrait, RegistryTrait;
    
    protected function __construct() 
    {
        $this->add('ROOT_DIR', dirname(dirname(__DIR__)));
        $dotenv = new Dotenv();
        $dotenv->loadEnv($this->getRootDir().'/.env');
        /** @var array $_ENV */
        foreach ($_ENV as $key => $value) {
            $this->add($key, $value);
        }
    }
    
    public function getEnv(): string 
    {
        return $this->get('APP_ENV');
    }
    
    public function getRootDir(): string
    {
        return $this->get('ROOT_DIR');
    }
    
    public function getBaseUri(): string
    {
        return $this->get('BASE_URI');
    }
    
    public function getPublicUri(): string
    {
        return $this->getBaseUri().$this->get('PUBLIC_DIR');
    }
       
    public function getConfigDir(): string
    {   
        return $this->getRootDir().'/config';
    }
    
    public function getTemplateDir(): string
    {
        return $this->getRootDir().'/templates';
    }
    
    public function getVarDir(): string
    {
        return $this->getRootDir().'/var';
    }
    
    public function getCacheDir(): string
    {
        return $this->getVarDir().'/cache';
    }
    
    public function getContentDir(): string
    {
        return $this->getVarDir().'/content';
    }
    
    public function getHtmlDir(): string
    {
        return $this->getVarDir().'/html';
    }
    
    public function getDBDir(): string 
    {
        return $this->getVarDir().'/db';
    }
    
    public function getTimeZone() 
    {
        return ($timezone = $this->get('TIMEZONE')) ? $timezone : date_default_timezone_get();
    }
}
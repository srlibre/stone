<?php
namespace Stone\Traits;

trait RegistryTrait
{
    /** @var array */
    protected $storage = [];
    
    public function set($key, $value)
    {
        $this->storage[$key] = $value;
        return $this;
    }
    
    public function add($key, $value)
    {
        return $this->set($key, $value);
    }
    
    public function get($key, $defaultValue = null)
    {
        return isset($this->storage[$key]) ? $this->storage[$key] : $defaultValue;
    }
    
    public function existsKey($key)
    {
        return isset($this->storage[$key]);
    }
    
    public function exists($value)
    {
        return in_array($value, $this->storage);
    }
    
    public function hasElements()
    {
        return !empty($this->storage) && (count($this->storage) > 0);
    }
    
    public function delete($key)
    {
        unset($this->storage[$key]);
        return $this;
    }
    
    public function serialize()
    {
        return serialize($this->storage);
    }    
}


<?php

namespace Stone\Traits;

trait SingletonTrait
{
    protected static $instance = null;

    private function __clone() {}
    protected function __construct() {}

    public static function create()
    {
        $className = static::class;
        if (is_null(self::$instance[$className])) {
            self::$instance[$className] = new static();
        }
        return self::$instance[$className];
    }
}

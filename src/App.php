<?php

namespace Stone;

use Symfony\Component\HttpFoundation\Request;
use Stone\Traits\SingletonTrait;
use Symfony\Component\Routing\Loader\PhpFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\HttpFoundation\InputBag;
use Twig\Loader\FilesystemLoader as TwigLoader;
use Twig\Environment as TwigEnv;
use Stone\Util\Config;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Twig\TwigFunction;
use Exception;
use Stone\DB\DB;
use Twig\Environment;

class App
{
    use SingletonTrait;

    /** @var Request */
    private $request;

    /** @var Config */
    private $config;

    /** @var RouteCollection */
    private $routes;

    /** @var RequestContext */
    private $requestContext;

    /** @var TwigEnv */
    private $template;

    /** @var UrlGenerator */
    private $urlGen;

    /** @var string */
    private $currentRoute;

    protected function __construct()
    {
        $this->request = Request::createFromGlobals();
        $this->config = Config::create();

        $loader = new PhpFileLoader(new FileLocator());
        $this->routes = $loader->load($this->config->getConfigDir().'/routes.php');


	    $this->requestContext = new RequestContext();

        $this->urlGen = new UrlGenerator($this->routes, $this->requestContext);

        $loader = new TwigLoader($this->config->getTemplateDir());
        $this->template = new TwigEnv($loader, $this->config->getEnv() !== 'local' ? ['cache' => $this->config->getCacheDir()] : []);

        $assetFunc = function ($path) {
            return $this->config->getPublicUri()."/{$path}";
        };

        $this->template->addFunction(new TwigFunction('asset', $assetFunc));

        $this->template->addFunction(new TwigFunction('css', function ($path) use ($assetFunc) {
            return call_user_func($assetFunc, "css/{$path}");
        }));

        $this->template->addFunction(new TwigFunction('image', function ($path) use ($assetFunc) {
            return $assetFunc("image/{$path}");
        }));

        $this->template->addFunction(new TwigFunction('js', function ($path) use ($assetFunc) {
            return call_user_func($assetFunc, "js/{$path}");
        }));

        $this->template->addFunction(new TwigFunction('url', function ($routeName, $parameters = []) {
            return $this->genUrl($routeName, $parameters);
        }));

        $this->template->addFunction(new TwigFunction('current_url', function ($parameters = []) {
            return $this->genUrl($this->currentRoute, $parameters);
        }));

        $this->template->addFunction(new TwigFunction('dump', function (...$args) {
            var_dump(...$args);
        }));

        $this->template->addFunction(new TwigFunction('timezone', function () {
            return $this->config->getTimeZone();
        }));

        $this->template->getExtension(\Twig\Extension\CoreExtension::class)->setTimezone($this->config->getTimeZone());

        DB::setDBDir($this->config->getDBDir());
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function getConfig(): Config
    {
        return $this->config;
    }

    public function getRoutes(): RouteCollection
    {
        return $this->routes;
    }

    public function getRequestContext(): RequestContext
    {
        return $this->requestContext;
    }

    public function getTemplate(): TwigEnv
    {
        return $this->template;
    }

    public function genUrl(string $routeName, array $parameters = []): string
    {
        return $this->config->getBaseUri().$this->urlGen->generate($routeName, $parameters);
    }

    public function getRoute($name): Route
    {
        return $this->routes->get($name);
    }

    public function getCurrentRoute(): string
    {
        return $this->currentRoute;
    }

    public function redirect(string $routeName, array $parameters = [])
    {
        header('Location: '. $this->genUrl($routeName, $parameters));
    }

    public function run()
    {
        try {
            $matcher = new UrlMatcher($this->routes, $this->requestContext);
            $parameters = $matcher->matchRequest($this->request);
            $controller = $parameters['_controller'];
            $this->currentRoute = $parameters['_route'];

            unset($parameters['_controller'], $parameters['_routes']);

            $this->request->query = new InputBag($parameters);
            $this->template->addGlobal('app', $this);
            $this->template->addGlobal('request', $this->getRequest());

            if (class_exists($controllerClass = $controller[0])) {
                $controllerObject = new $controllerClass($this);
                if (isset($controller[1])) {
                    $controllerMethod = $controller[1];
                    $output = call_user_func([$controllerObject, $controllerMethod]);
                } else {
                    $output = $controllerObject();
                }
            } elseif (is_callable($controller)) {
                $output = call_user_func($controller[0], $this);
            } else {
                throw new Exception('The route handler must be callable!');
            }

            if (is_string($output)) {
                echo $output;
            } elseif ($output instanceof Response) {
                echo $output->getContent();
            }
        } catch (Exception $e) {
            $errorController = new Controller\AppErrorController($this);
            $errorController($e);
        }
    }
}

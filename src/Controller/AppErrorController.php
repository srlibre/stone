<?php

namespace Stone\Controller;

use Exception;

class AppErrorController extends BaseController
{
    public function __invoke(Exception $e)
    {           
        $this->display('error.twig', ['error' => [
                'title'       => 'App Error!',
                'message'     => $e->getMessage(),
                'stack_trace' => $e->getTrace()
            ]
        ]);
    }
}
<?php

namespace Stone\Controller;

use Symfony\Component\Filesystem\Exception\{IOExceptionInterface, IOException};
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;

use League\CommonMark\CommonMarkConverter;

use Stone\DB\DB;

use SleekDB\Exceptions\InvalidArgumentException;

use Cocur\Slugify\Slugify;

class PostController extends BaseController
{

    public function list()
    {
        $metadata = DB::getMetadataRepository();
        return $this->render('list.twig', ['metadata' => $metadata->getAll(100, ['language', '=', $this->app->getConfig()->get('DEFAULT_LANG')])]);
    }

    public function view()
    {
        $metadataRepository = DB::getMetadataRepository();

        $post = $metadataRepository->findByCriteria(['slug', '=', $this->request->get('slug')]);

        $post['html'] = @file_get_contents($this->app->getConfig()->getHtmlDir().'/'.$post['html_path']);

        return $this->render('view.twig', ['post' => $post]);
    }

    public function new()
    {
        return $this->render('edit.twig', ['action' => 'NEW']);
    }

    public function edit()
    {
        $metadataRepository = DB::getMetadataRepository();

        $markdown = $metadataRepository->findByCriteria(['slug', '=', $this->request->get('slug')]);
        $markdown['content'] = @file_get_contents($this->app->getConfig()->getContentDir().'/'.$markdown['markdown_path']);

        return $this->render('edit.twig', ['markdown' => $markdown, 'action' => 'EDIT']);
    }

    public function save()
    {
        $filesystem = new Filesystem();
        try {
            if (!empty($markdown = $this->request->get('markdown'))) {

                $fileContent  = $markdown['content'];
                $tags         = explode(',', $markdown['tags']);
                $markdownFile = $markdown['title'].'.md';
                $htmlFile     = $markdown['title'].'.html';

                $converter = new CommonMarkConverter();
                $html = $converter->convertToHtml($fileContent);

                $filesystem->dumpFile($this->app->getConfig()->getContentDir().'/'.$markdownFile, $fileContent);
                $filesystem->dumpFile($this->app->getConfig()->getHtmlDir().'/'.$htmlFile, $html);

                $slug = (new Slugify())->slugify($markdown['title']);
                $language = $markdown['language'];

                $metadata['title']         = $markdown['title'];
                $metadata['markdown_path'] = $markdownFile;
                $metadata['html_path']     = $htmlFile;
                $metadata['slug']          = $slug;
                $metadata['tags']          = $tags;
                $metadata['language']      = $language;

                $translationId             = $markdown['translations_id'] ?? null;

                $metadataRepository = DB::getMetadataRepository();

                if (!empty($markdown['_id']) && ($document = $metadataRepository->findById($markdown['_id']))) {
                    $metadata['_id'] = $document['_id'];
                    // Delete old files when title changed
                    if ($document['slug'] != $slug) {
                        @unlink($this->app->getConfig()->getContentDir().'/'.$document['markdown_path']);
                        @unlink($this->app->getConfig()->getHtmlDir().'/'.$document['html_path']);
                    }

                    if (empty($translationId)) {
                        $translationId = $document['translationsId'] ?? null;
                    }
                }

                if (!empty($translationId)) {
                    $translation['_id'] = $translationId;
                }

                $translation[$language] = $slug;

                //\var_dump($translation); return;
                $translationsRepository = DB::getTranslationsRepository();
                $translationsId = $translationsRepository->add($translation);

                $metadata['translationsId'] = $translationsId;

                $id = $metadataRepository->add($metadata);

                if ($this->isAjaxRequest()) {
                    return new JsonResponse(['result' => 'OK', '_id' => $id]);
                }

                if ($this->request->get('button_action') == 'SAVE_VIEW') {
                    return $this->app->redirect('markdown_view', ['slug' => $slug]);
                }

                return $this->app->redirect('markdown_home');
            }
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating your directory at ".$exception->getPath();
        }
    }

    public function delete()
    {
        $metadataRepository = DB::getMetadataRepository();
        $deleted = $metadataRepository->deleteByCriteria(['slug', '=', $this->request->get('slug')]);

        foreach ($deleted as $item) {
            @unlink($this->app->getConfig()->getContentDir().'/'.$item['markdown_path']);
            @unlink($this->app->getConfig()->getHtmlDir().'/'.$item['html_path']);
        }

        /*if ($markdownMetadata = $metadataRepository->findByCriteria(['slug', '=', $this->request->get('slug')])) {
            $translationsId = $markdownMetadata['translationsId'];
            if ($result = $metadataRepository->deleteByCriteria(['translationsId', '=', $translationsId])) {
                foreach ($result as $deleted) {
                    @unlink($this->app->getConfig()->getContentDir().'/'.$deleted['markdown_path']);
                    @unlink($this->app->getConfig()->getHtmlDir().'/'.$deleted['html_path']);
                }
                DB::getTranslationsRepository()->deleteById($translationsId);
            }
        }*/
        return $this->redirect('markdown_home');
    }

    public function ajax()
    {
        return new JsonResponse(['result' => 'OK', '_id' => 1]);
    }
}

<?php
namespace Stone\Controller;

use Stone\App;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Request;

class BaseController
{
    /** @var App */
    protected $app;

    /** @var Environment */
    protected $template;

    /** @var Request*/ 
    protected $request;
    
    public function __construct(App $app) 
    {
        $this->app = $app;
        $this->template = $app->getTemplate();
        $this->request = $app->getRequest();
    }
    
    public function display(string $template, array $params = [])
    {
        $this->template->display($template, $params);
    }
    
    public function render(string $template, array $params = [])
    {
        return $this->template->render($template, $params);
    }
    
    public function genUrl(string $routeName) 
    {
        return $this->app->genUrl($routeName);
    }

    public function isAjaxRequest()
    {
        return $this->request->isXmlHttpRequest();
    }
    
    public function redirect(string $route, array $parameters = []) 
    {
        return $this->app->redirect($route, $parameters);
    }
    
    public function renderError(array $error)
    {
        return $this->template->render('error.twig', ['error' => $error]);
    }
}


<?php
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use Stone\Controller\PostController;

return function (RoutingConfigurator $routes) {
    $routes->add('post_home', '/')
        // the controller value has the format [controller_class, method_name]
        ->controller([PostController::class, 'list']);

    // if the action is implemented as the __invoke() method of the
    // controller class, you can skip the 'method_name' part:
    // ->controller(BlogController::class)
    $routes->add('post_render', '/render/{file}')
        ->controller([PostController::class, 'render']);

    $routes->add('post_new', '/new/{translations_id}')
        ->controller([PostController::class, 'new'])
        ->defaults(['translations_id' => ''])
        ->requirements(['translations_id' => '\d+']);

    $routes->add('post_save', '/save')
        ->controller([PostController::class, 'save']);

    $routes->add('post_view', '/view/{slug}')
        ->controller([PostController::class, 'view']);

    $routes->add('post_edit', '/edit/{slug}')
        ->controller([PostController::class, 'edit']);

    $routes->add('post_delete', '/delete/{slug}')
        ->controller([PostController::class, 'delete']);

    $routes->add('post_ajax', '/ajax')
        ->controller([PostController::class, 'ajax']);
};
